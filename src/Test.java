
public class Test {
	public static void main(String[] args) {
		MyDinamicArray myDinamicArray = new MyDinamicArray();
		
		myDinamicArray.add(6);
		myDinamicArray.add(10);
		myDinamicArray.add(7);
		
		myDinamicArray.add(15, 1);
		System.out.println("Hello");
		
		MyLinkedList linkedList = new MyLinkedList();
		linkedList.add(6);
		linkedList.add(10);
		linkedList.add(7);
		
		linkedList.add(15, 1);
		System.out.println("Hello");
	}
}
