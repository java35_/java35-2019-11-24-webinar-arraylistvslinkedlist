import java.util.Arrays;

public class MyDinamicArray {
	private Integer[] array;
	private int size;
	
	public MyDinamicArray() {
		array = new Integer[10];
		size = 0;
	}
	
	public boolean add(Integer value, int index) {
		if (index < 0 || index > size)
			return false;
		
		if (size == array.length)
			allocateMemory();
		
		// Add last - O(n) (Am - O(1))
		if (index == size) {
			array[index] = value;
			size++;
			return true;
		}
		
		// Add first or middle - O(n)
		for (int i = size; i > index; i--) {
			array[i] = array[i - 1];
		}
		array[index] = value;
		size++;
		return true;
	}
	
	public boolean add(Integer value) {
		return add(value, size);
	}
	
	private void allocateMemory() {
//		Integer[] newArray = new Integer[array.length * 2];
//		for (int i = 0; i < array.length; i++) {
//			newArray[0] = array[0];
//		}
		
		Integer[] newArray = Arrays.copyOf(array, array.length * 2);
		array = newArray;
	}
}
