public class MyLinkedList {
	int size;
	Node firstNode;
	Node lastNode;
	
	class Node {
		Node nextNode;
		Node previousNode;
		Integer value;
		
		public Node(Integer value) {
			this.value = value;
		}
	}
	
	public MyLinkedList() {
		size = 0;
	}
	
	public boolean add(Integer value, int index) {
		if (index < 0 || index > size)
			return false;
		
		Node newNode = new Node(value);
		
		if (size == 0) {
			firstNode = newNode;
			lastNode = newNode;
			size++;
			return true;
		}
		
		// Add first
		if (index == 0) {
			newNode.nextNode = firstNode;
			firstNode.previousNode = newNode;
			firstNode = newNode;
			size++;
			return true;
		}
		
		// Add last
		if (index == size) {
			newNode.previousNode = lastNode;
			lastNode.nextNode = newNode;
			lastNode = newNode;
			size++;
			return true;
		}
		
		// Add middle
		Node cursor = firstNode;
		for (int i = 1; i <= index; i++) {
			cursor = cursor.nextNode;
		}
		
		newNode.previousNode = cursor.previousNode;
		newNode.nextNode = cursor;
		
		newNode.previousNode.nextNode = newNode;
		newNode.nextNode.previousNode = newNode;
		
		size++;
		return true;
			
	}
	
	public boolean add(Integer value) {
		return add(value, size);
	}
}
